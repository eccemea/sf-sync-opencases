//#region required npm modules
const express = require('express');
const app = express();
const sf = require('node-salesforce');
const gc = require('./gc');
const config = require('./config.json');
//#endregion

//#region local variables
let _conn;
let cases = {};
let users = {};

//#endregion

//#region Skill-Routing logic
const backlog = 5; // treshold where to apply or remove skill from User
const skillId = '22e18ea4-d6ae-46ae-a693-1aecb09f237f'; // skillId Backlog empty to be applied once backlog treshold is reached
const sfQuery =
  "SELECT Owner.Username, count(status) from Case WHERE status = 'New' OR status = 'Working' GROUP BY Owner.Username";
const intervalSec = 15; // how often Sync changes with SF

//#endregion

// Initiate main Thread
mainThread();

function mainThread() {
  console.log('Try to login to the SF account...');

  _conn = new sf.Connection({
    loginUrl: 'https://login.salesforce.com',
  });

  _conn.login(
    config.salesforce.username,
    config.salesforce.password + config.salesforce.token,
    function (err) {
      if (err) {
        console.log(err.message);
        reject(false);
        return;
      }
      console.log('Logged-In to SF account');
      gc.login(
        config.genesysCloud.clientId,
        config.genesysCloud.clientSecret,
        'mypurecloud.ie'
      )
        .then(() => {
          // Collect UserIds from GC
          gc.getUsers().then((resp) => {
            console.log('All users from GC loaded...');
            if (resp) {
              resp.forEach((user) => {
                users[user.email] = user.id;
              });
              console.log('Initial sync for OpenCases...');
              // Get Initial OpenCases from SF
              salesforceQuery(sfQuery).then((resp) => {
                if (resp && resp.totalSize > 0 && resp.records.length > 0) {
                  // Analyze result from SF
                  resp.records.forEach((item) => {
                    if (item.Username.includes('@') && users[item.Username]) {
                      // Valid User, check for OpenCases
                      console.log(`Sync Skill for user ${item.Username}...`);
                      cases[item.Username] = item.expr0;
                      let userId = users[item.Username];
                      if (item.expr0 > backlog) {
                        gc.removeRoutingSkillFromUser(userId, skillId); // Remove `Backlog empty`
                      } else {
                        gc.addRoutingSkillToUser(userId, skillId); // Add `Backlog empty`
                      }
                    } else
                      console.error(
                        `User (${item.Username}) not defined in GC - skip`
                      );
                  });
                }
                console.log(cases);

                // Start Timer
                setInterval(() => {
                  console.log('########## Sync() ##########');
                  salesforceQuery(sfQuery).then((resp) => {
                    if (resp && resp.totalSize > 0 && resp.records.length > 0) {
                      resp.records.forEach((item) => {
                        if (
                          item.Username.includes('@') &&
                          users[item.Username]
                        ) {
                          if (item.expr0 <= backlog) {
                            if (cases[item.Username] <= backlog)
                              cases[item.Username] = item.expr0;
                            else {
                              cases[item.Username] = item.expr0;
                              gc.addRoutingSkillToUser(
                                users[item.Username],
                                skillId
                              );
                            }
                          } else if (item.expr0 > backlog) {
                            if (cases[item.Username] > backlog)
                              cases[item.Username] = item.expr0;
                            else {
                              cases[item.Username] = item.expr0;
                              gc.removeRoutingSkillFromUser(
                                users[item.Username],
                                skillId
                              );
                            }
                          }
                        } else
                          console.error(`Unknown user: ${item.Username}, skip`);
                      });
                    }
                  });
                  console.log('local copy of SF OpenCases');
                  console.log(cases);
                }, 1000 * intervalSec);
              });
            }
          });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  );
}

function salesforceQuery(queryString) {
  return new Promise(function (resolve, reject) {
    _conn.query(queryString, function (err, result) {
      if (err) {
        reject(err);
        console.error(err);
        return;
      }
      //console.log(result);
      resolve(result);
    });
  });
}

let server = app.listen(3001, function () {
  var host = server.address().address;
  var port = server.address().port;
  console.log('Connector listening at http://%s:%s', host, port);
});
