let platformClient = require('purecloud-platform-client-v2');
let client = platformClient.ApiClient.instance;

// Login to the PureCloud and retreive Users and Queues
function login(clientId, clientSecret, env) {
  client.setEnvironment(env);

  console.log('Try to login to GenesysCloud...');
  console.log(`env         : ${env}`);
  console.log(`clientId    : ${clientId}`);
  console.log(`clientSecret: ${clientSecret.substring(0, 10)}(...)`);

  return new Promise(function (resolve, reject) {
    try {
      client
        .loginClientCredentialsGrant(clientId, clientSecret)
        .then(function () {
          console.log('Logged-In to PureCloud!');

          let resp = {
            token: client.authData.accessToken,
            expiryTime: client.authData.tokenExpiryTime,
          };
          client.setAccessToken(resp.token);
          resolve(resp);
        })
        .catch(function (err) {
          console.log(err);
          reject();
        });
    } catch (err) {
      console.log(err);
      reject();
    }
  });
}

function getUsers() {
  return new Promise(function (resolve, reject) {
    let apiInstance = new platformClient.UsersApi();

    let opts = {
      pageSize: 100, // Number | Page size
      pageNumber: 1, // Number | Page number
      //expand: [], // [String] | Which fields, if any, to expand
      state: 'active', // String | Only list users of this state
    };

    apiInstance
      .getUsers(opts)
      .then((data) => {
        resolve(data.entities);
      })
      .catch((err) => {
        console.log('There was a failure calling getUsers');
        console.error(err);
        reject(err);
      });
  });
}

function addRoutingSkillToUser(_userId, _skillId) {
  return new Promise(function (resolve, reject) {
    let apiInstance = new platformClient.RoutingApi();
    console.log('addRoutingSkillToUser', _userId);
    let body = {
      id: _skillId,
      proficiency: 5,
    };

    apiInstance
      .postUserRoutingskills(_userId, body)
      .then(() => {
        console.log(`skill applied for user ${_userId}`);
        resolve();
      })
      .catch((err) => {
        console.log('There was a failure calling postUserRoutingskills');
        console.error(err);
        reject();
      });
  });
}

function removeRoutingSkillFromUser(_userId, _skillId) {
  return new Promise(function (resolve, reject) {
    let apiInstance = new platformClient.RoutingApi();
    console.log('removeRoutingSkillFromUser', _userId);

    apiInstance
      .deleteUserRoutingskill(_userId, _skillId)
      .then(() => {
        console.log(`skill removed from user ${_userId}`);
        resolve();
      })
      .catch((err) => {
        console.log(
          `There was a failure calling deleteUserRoutingskill for userId ${_userId}`
        );
        console.error(err);
        reject();
      });
  });
}

module.exports = {
  login,
  getUsers,
  addRoutingSkillToUser,
  removeRoutingSkillFromUser,
};
